package main

func main() {
	println(sum(1, 2))
}
func sum(x, y int) (a, b int) { //this is the example of named retun values and multiple retun values 
	a = x + y
	b = x - y
	return a, b

}
