package main

import "fmt"

func main() { //conditional

	if sum := 100; sum > 100 { //we can also do like this,
		//inilizing and condion checking in same line

		fmt.Printf("true")
	} else {
		fmt.Println("false")
	}
}
