package main

func main() {
	//for is the only one loop in go and go has a infinite loop but its not recommanded
	sum := 0
	for i := 0; i < 10; i++ {
		sum = sum + i

	}
	println(sum)
}
