# Go lang
**1. Basic Hello world program**

### - Algorithm
```
step 1: start
step 2: print hello world
step 3: stop
```

### - Code
```
package main

import "fmt"

func main() {
	fmt.Println("Hello world") //print statement
}
```

### - Output
```
Hello world
```
---
**2. Variables**

### -Algorithm
```
step 1: start
step 2: declare a variable with initializer
step 3: declare a short variable
step 3: print all variables
step 4: stop
```

### -Code
```
package main

import "fmt"

var x, y, z bool // Variable Declaration

var a = 10 // initializing 

func main() {
	b := 20 // Short Variable delarion (*this type of variable cannot be declare outside the Main function)
	fmt.Println(b, a, x, y, z)
}
```

### -Output
```
20 10 false false false
```
---
**3. Loop**

### -Algorithm
```
step 1: start
step 2: declare a sum variable
step 3: declare a for loop
step 4: perform sum+=i
step 5: stop
```

### -Code
```
package main

func main() {
	//for is the only one loop in go and go has a infinite loop but its not recommanded
	sum := 0
	for i := 0; i < 10; i++ {
		sum = sum + i

	}
	println(sum)
}
```

### -Output
```
45
```
---

**4. Conditional Statements**
### -Algorithm
```
step 1: start
step 2: declare a variable and intilize a value
step 3: use if statements and check the value
step 5: exicute the statement of if
step 6: stop
```

### -Code
```
package main

import "fmt"

func main() { //conditional

	if sum := 100; sum > 100 { //we can also do like this,
		//inilizing and condion checking in same line

		fmt.Printf("true")
	} else {
		fmt.Println("false")
	}
}

```

### -Output
```
false
```

**5. Defer and Stack Defer program**

### -Algorithm
```
step 1: start
step 2: declare a variable and intilize a value
step 3: use if statements and check the value
step 5: exicute the defer statement last
step 6: stop
```

### -code
```
package main

//defer and Stacking defer -LIFO(Last In First Out)

func main() {
	for i := 0; i < 10; i++ {
		defer println(i)
	}
	defer println("defer testing") // Stack defer

}

```

### -Output
```
defer testing
9
8
7
6
5
4
3
2
1
0
```
---

**6. Basic Function program**

### -Algorithm
```
step 1: start
step 2: declare a function with 2 intiger parametes
step 3: call sum Function With Sending 2 intiger values
step 5: print the sum value
step 6: stop
```

### -Code
```
package main

func add(x, y int) int {
	return x + y
}
func main() {
	println(add(1, 2))
}
```

### -Output
```
3
```

**7. named retun values and multiple retun values program**

### algorithm
```
step 1: start
step 2: declare a function with 2 intiger parametes and Retunrs 2 results with name
step 3: call sum Function With Sending 2 intiger values
step 5: print the sum value
step 6: stop
```

### -Code
```
package main

func main() {
	println(sum(1, 2))
}
func sum(x, y int) (a, b int) { //this is named retun values and multiple retun values example
	a = x + y
	b = x - y
	return a, b

}

```

### -Output
```
3,-1
```
---
