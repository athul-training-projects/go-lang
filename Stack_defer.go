package main

//defer and Stacking defer -LIFO(Last In First Out)

func main() {
	for i := 0; i < 10; i++ {
		defer println(i)
	}
	defer println("defer testing") // Stack defer

}
