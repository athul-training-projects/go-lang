package main

import "fmt"

var x, y, z bool // Variable Declaration

var a = 10 // initializing

func main() {
	b := 20 // Short Variable delarion (*this type of variable cannot be declare outside the Main function)
	fmt.Println(b, a, x, y, z)
}
